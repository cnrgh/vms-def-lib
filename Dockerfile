ARG DOCKER_RUBY_VERSION=3.2
FROM ruby:${DOCKER_RUBY_VERSION}

# Set local variables
ARG dir=/workdir

# Copy files
COPY Makefile install-deps generate-inventory .rubocop.yml vms_yml_reader.rb $dir/
COPY bash-lib/ $dir/bash-lib/
COPY tests/ $dir/tests/

# Change dir
WORKDIR $dir

# Install requirements
RUN ./install-deps

# Test
RUN make check test
