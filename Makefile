RB_TEST_FILES=$(wildcard tests/test_*.rb)

all:

clean:

test:
	bash-lib/runtests tests
	ruby -Ilib:test $(RB_TEST_FILES)

check:
	rubocop

docker:
	docker build -t test-vms-lib .

.PHONY: all clean test check docker
