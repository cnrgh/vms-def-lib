# frozen_string_literal: true

require "minitest/autorun"
require_relative "../vms_yml_reader"

# Test Vms class
class TestVms < Minitest::Test
  def test_001_missing_name_attribute
    error = assert_raises(ArgumentError) do
      Vms.new(file: File.join(__dir__, 'res', 'failing_vms_001.yml'))
    end

    assert_match "Missing mandatory attribute 'name' for VM at index 0.", error.message
  end

  def test_002_invalid_box_attribute
    error = assert_raises(ArgumentError) do
      Vms.new(file: File.join(__dir__, 'res', 'failing_vms_002.yml'))
    end

    assert error.message.start_with?("Invalid attribute 'box.box_version' for VM 'plantaquatix'.")
  end

  def test_003_missing_mandatory_box_attribute
    error = assert_raises(ArgumentError) do
      Vms.new(file: File.join(__dir__, 'res', 'failing_vms_003.yml'))
    end

    assert error.message.start_with?("Missing mandatory attribute 'box.account' for VM 'plantaquatix'.")
  end

  def test_010_two_hosts_with_two_groups
    vms = Vms.new(file: File.join(__dir__, 'res', 'vms_010.yml'))
    output_file = File.join(__dir__, 'wrk', 'inventory_010.yml')
    ref_file = File.join(__dir__, 'res', 'inventory_010.yml')
    FileUtils.mkdir_p(File.join(__dir__, 'wrk'))
    vms.write_inventory(file: output_file)
    assert FileUtils.compare_file(ref_file, output_file)
  end

  def test_020_two_hosts_with_same_group
    vms = Vms.new(file: File.join(__dir__, 'res', 'vms_020.yml'))
    output_file = File.join(__dir__, 'wrk', 'inventory_020.yml')
    ref_file = File.join(__dir__, 'res', 'inventory_020.yml')
    FileUtils.mkdir_p(File.join(__dir__, 'wrk'))
    vms.write_inventory(file: output_file)
    assert FileUtils.compare_file(ref_file, output_file)
  end

  def test_030_single_host_no_box
    vms = Vms.new(file: File.join(__dir__, 'res', 'vms_030.yml'))
    output_file = File.join(__dir__, 'wrk', 'inventory_030.yml')
    ref_file = File.join(__dir__, 'res', 'inventory_030.yml')
    FileUtils.mkdir_p(File.join(__dir__, 'wrk'))
    vms.write_inventory(file: output_file)
    assert FileUtils.compare_file(ref_file, output_file)
  end
end
