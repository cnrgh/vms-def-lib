# vi: se ft=bash

SCRIPT_DIR=$(dirname "${BASH_SOURCE[0]}")
RES_DIR="$SCRIPT_DIR/res"
WRK_DIR="$SCRIPT_DIR/wrk"
SCRIPT_NAME=generate-inventory
SCRIPT="$SCRIPT_DIR/../$SCRIPT_NAME"

tt_context "Testing $SCRIPT_NAME script"

function test_010_help {

    local -i i=0

    tt_expect_success "$SCRIPT" -h || return $((++i))
    tt_expect_success "$SCRIPT" --help || return $((++i))

    return 0
}

function test_100_generate_inventory {

    local -i i=0

    tt_expect_success mkdir -p "$WRK_DIR" || return $((++i))
    for f in "$RES_DIR"/vms_*.yml ; do
        local inv_file="${f//vms_/inventory_}"
        local out_file
        out_file="$WRK_DIR/$(basename "$inv_file")"
        tt_expect_success "$SCRIPT" -i "$f" -o "$out_file" || return $((++i))
        tt_expect_same_files "$inv_file" "$out_file" || return $((++i))
    done

    return 0
}
