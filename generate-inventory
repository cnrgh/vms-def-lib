#!/usr/bin/env ruby
# vi: tw=80
# frozen_string_literal: true

require "#{__dir__}/vms_yml_reader"
require 'optparse'
require 'ostruct'

Args = Struct.new(:vms_file, :inventory_file)

def read_args

  args = Args.new(vms_file: 'vms.yml', inventory_file: 'inventory.yml')

  OptionParser.new do |opt|
    opt.on('-i', '--input-file VMS_FILE',
           "The VMs YAML file to read. Default is \"#{args.vms_file}\".") \
      { |o| args.vms_file = o }
    opt.on('-o', '--output-file VMS_FILE',
           'The path of the inventory YAML file to write. ' \
           "Default is \"#{args.inventory_file}\"") \
          { |o| args.inventory_file = o }
  end.parse!

  args
end

def main
  args = read_args
  vms = Vms.new(file: args.vms_file)
  vms.write_inventory(file: args.inventory_file)
end

__FILE__ == $PROGRAM_NAME && main
