# vms-lib

A Ruby library for configuring Vagrant environments and generating Ansible inventories from YAML definition files.

## Installation

To use `vms-lib`, we recommend adding the repository as a Git submodule in your project.
You can also simply clone the repository.

## YAML definition file

A YAML file defines custom Virtual Machines using YAML objects.
At the root of this file, a list of `VirtualMachine` objects is defined.

### `VirtualMachine` attributes:

- `name` *(str)*: **mandatory**, this is the hostname of the machine,
- `description` *(str)*: **optional**, this is the description of the machine
  (purely for documentation purposes as it is not used by the library nor Vagrant),
- `ip` *(str)*: **optional**, this is the IP assigned to the box in the private network and
  to the Ansible host in the inventory file,
- `cpus` *(int)*: **optional**, this is the number of CPUs allocated to the VM,
- `memory` *(int)*: **optional**, this is the amount of memory allocated to the VM (in MB),
- `box` *(Box)*: **optional**: this is the box configuration that Vagrant will use to provision the machine.
  If `vms-lib` is called from a `Vagrantfile`, omitting this attribute will cause a Vagrant error.
  However, not setting a `box` can be useful if you only want to use the
  Ansible inventory file generator script (`generate-inventory`),
- `ansible` *(Ansible)*: **optional**, this is the Ansible provisioning configuration,
- `disable_shared_folder` *(bool)*: **optional**, this disables sharing the project directory
  (the directory with the Vagrantfile) with `/vagrant` on the VM (default: `false`),
- `disable_log` *(bool)*: **optional**, this disables the VM's serial port, preventing logs from being sent to the host
  (default: `false`).

### `Box` attributes:

- `account` *(str)*: **mandatory**, this is the account providing the box,
- `tag` *(str)*: **mandatory**, this is the tag of the box,
- `version` *(str)*: **optional**, this is the version constraint string,
  used to determine the version of the box to use (default: `>= 0`, i.e. the latest version available),
- `type` *(str)*: **optional**, this is used to define special actions required before
  provisioning the VM with Ansible. Currently, the only type supported is `archlinux`,
  which tells Vagrant to install `python` on an Arch Linux VM before running Ansible.

### `Ansible` attributes:

- `playbook` *(str)*: **optional**, this is the path to the Ansible playbook used to provision the VM
  (default: `provisioning/playbook.yml`),
- `groups` *(list[str])*: **optional**, this is a list of groups the VM belongs to (default: `[ungrouped]`),
- `vars` *(dict[str])*: **optional**, this is a dictionary containing variables to
  include in the generated inventory file.

## Usage

### Vagrantfile

To use `vms-lib` in a Vagrantfile, import the main module `vms_yml_reader`.
Then, instantiate a `Vms` object and call the function `create_vagrant_machines`.
It will look for a YAML definition file named `vms.yml` in the current directory.

```ruby
# frozen_string_literal: true

require_relative 'vms-lib/vms_yml_reader'

vms = Vms.new
vms.create_vagrant_machines
```

> Depending on where the `vms-lib` directory is located in your project,
> you may need to change the path given to `require_relative`.

Then, create a file `vms.yml`:

```yaml
---
- name: rocky9-test
  description: A simple machine to try out Rockylinux 9.
  ip: 192.168.73.10
  memory: 2048
  cpus: 1
  box:
    account: rockylinux
    tag: "9"
- name: archlinux-test
  description: A simple machine to try out Archlinux.
  ip: 192.168.73.11
  memory: 2048
  cpus: 1
  box:
    account: archlinux
    tag: "archlinux"
    version: "> 20230000, <= 20240000"

```

To create both VMs, simply run `vagrant up`.
You can also start a specific VM by specifying its name: `vagrant up rocky9-test`.

### Command-line script: generate-inventory

`vms-lib` is also able to only generate Ansible inventories with the `generate-inventory` CLI script.

It supports the following CLI options:

- `-i` / `--input-file`: the YAML definition file to read (default: `vms.yml`),
- `-o` / `--output-file`: the path of the inventory file to write (default: `inventory.yml`).

> This script does not use Vagrant, so no VMs will be created.
> As such, you can omit the `box` attribute in your VM definition.
