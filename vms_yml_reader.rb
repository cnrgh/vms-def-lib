# frozen_string_literal: true

require 'yaml'
require 'logger'

LOGGER = Logger.new($stdout, level: Logger::INFO)

# Virtual Machines class
class Vms
  # Loading of YAML definition of virtual machines.
  # Generation of Ansible inventory file.
  # Creation of Vagrant machines.
  
  def initialize(file: 'vms.yml')
    @vms = YAML.load_file(file)
    self._check_vms
  end
  
  def _check_vms
    mandatory_box_attrs = %w[account tag]
    allowed_box_attrs = mandatory_box_attrs + %w[version type]

    @vms.each_with_index do |vm, index|
      # Check required fields.
      raise ArgumentError, "Missing mandatory attribute 'name' for VM at index #{index}." unless vm.key?('name')

      # Skip to next VM if there is no box defined.
      next unless vm.key?('box')

      # Check that only allowed attributes for 'box' are defined.
      vm['box'].each_key do |key|
        next if allowed_box_attrs.include?(key)

        raise ArgumentError,
              "Invalid attribute 'box.#{key}' for VM '#{vm['name']}'. " \
              "Allowed attributes: #{allowed_box_attrs.join(', ')}."
      end

      # Check that all mandatory attributes for 'box' are defined.
      mandatory_box_attrs.each do |attrs|
        next if vm['box'].key?(attrs)

        raise ArgumentError,
              "Missing mandatory attribute 'box.#{attrs}' for VM '#{vm['name']}'."
      end
    end

    nil
  end

  def _cfg_ansible(cfg:, vm:, inventory_file:)
    if vm['box'].key?('type')
      case vm['box']['type']
      when 'archlinux'
        # When box type is archlinux, install Python for Ansible.
        LOGGER.debug("Using custom provisioning shell script for type 'archlinux'.")
        cfg.vm.provision "Install Python.",
          type: "shell", privileged: true,
          inline: "pacman -Sy --noconfirm python"
      else
        LOGGER.debug("Nothing to do for type '#{vm['box']['type']}'.")
      end
    end

    # Enable Ansible provisioning
    cfg.vm.provision :ansible do |ansible|
      ansible.playbook = vm['ansible'].key?('playbook') ? 
        vm['ansible']['playbook'] : "provisioning/playbook.yml"
      ansible.inventory_path = inventory_file

      # Forces Ansible version, otherwise Vagrant gathers an unknown Ansible
      # version or a falls back to version 1.8.
      ansible.compatibility_mode = "2.0"
    end

    nil
  end

  def _set_base_cfg(cfg:, vm:)
    # Build box name.
    cfg.vm.box = "#{vm['box']['account']}/#{vm['box']['tag']}"
    # If the box version is not set, use the latest version available.
    cfg.vm.box_version = vm['box'].key?('version') ? vm['box']['version'] : ">= 0"

    cfg.vm.hostname = vm['name']
    cfg.vm.provider :virtualbox do |vb|
      vb.name = vm['name']
      vm.key?('memory') && (vb.memory = vm['memory'])
      vm.key?('cpus') && (vb.cpus = vm['cpus'])

      # Disable log file
      vm.key?('disable_log') && vm['disable_log'] &&
        vb.customize([ "modifyvm", :id, "--uartmode1", "disconnected" ])
    end
  end

  def create_vm(vm:, vagrant_cfg:, inventory_file:)

    # Define machine
    vagrant_cfg.vm.define vm['name'] do |cfg|
      
      # Base configuration
      self._set_base_cfg(cfg: cfg, vm: vm)

      # Disable shared folder
      vm.key?('disable_shared_folder') && vm['disable_shared_folder'] &&
        cfg.vm.synced_folder('.', '/vagrant', disabled: true)

      # Network
      vm.key?('ip') && cfg.vm.network("private_network", ip: vm['ip'])

      # Ansible
      if vm.key?('ansible')

        # Configure Ansible provisioning
        self._cfg_ansible(vm: vm, cfg: cfg, inventory_file: inventory_file)

        # Tell Ansible to format output on error.
        ENV['ANSIBLE_STDOUT_CALLBACK'] = 'debug'
      end
    end

    nil
  end

  def create_vagrant_machines
    Vagrant.configure(2) do |vagrant_cfg|

      inventory_file = "inventory.yml"
      self.write_inventory(file: inventory_file)

      # Loop on all VMs
      @vms.each do |vm|
        next unless vm.key?('box') # Skip Vagrant VM creation if no box was provided.

        self.create_vm(vm: vm, vagrant_cfg: vagrant_cfg,
                       inventory_file: inventory_file)
      end
    end
  end

  def each
    # rubocop:disable Style/ExplicitBlockArgument
    @vms.each { |vm| yield vm }
    # rubocop:enable Style/ExplicitBlockArgument
  end

  def _write_grp_cfg(vm:, grp:, inventory:)

    cfg = {}

    # Set variables
    vm['ansible'].key?('vars') && (cfg = vm['ansible']['vars'])
    
    # Set IP
    vm.key?('ip') && (cfg['ansible_host'] = vm['ip'])

    # Set machine configuration inside inventory
    inventory['all']['children'].key?(grp) ||
      (inventory['all']['children'][grp] = { 'hosts' => {} })
    inventory['all']['children'][grp]['hosts'][vm['name']] = cfg

    nil
  end

  def write_inventory(file: "inventory.yml")
    
    # Create inventory hash
    inventory = {'all' => {'children' => {}}}

    # Loop on all VMs
    @vms.each do |vm|

      # Has Ansible config?
      vm.key?('ansible') || next

      # Get groups
      groups = vm['ansible'].key?('groups') ? vm['ansible']['groups'] :
        ['ungrouped']

      # Write configuration for each group
      groups.each do |grp|
        self._write_grp_cfg(vm: vm, grp: grp, inventory: inventory)
      end
    end

    # Write inventory as YAML
    File.write(file, YAML.dump(inventory))

    nil
  end
end
